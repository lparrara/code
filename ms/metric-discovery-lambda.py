import boto3
import json
import os
from datetime import datetime, timedelta

class MetricDiscoveryHandler:
    def __init__(self):
        # Initialize AWS clients
        self.sts_client = boto3.client('sts')
        self.dynamodb = boto3.resource('dynamodb')
        self.s3 = boto3.client('s3')
        self.cloudwatch = boto3.client('cloudwatch')  # Default to current account
        
        # Environment variables (configure these in Lambda settings)
        self.roles_table_name = os.environ.get('ROLES_TABLE_NAME')
        self.profile_table_name = os.environ.get('PROFILE_TABLE_NAME')
        self.resources_table_name = os.environ.get('RESOURCES_TABLE_NAME')
        self.metrics_bucket = os.environ.get('METRICS_S3_BUCKET')
        
        # Get current account ID
        self.current_account_id = self.sts_client.get_caller_identity()['Account']

    def get_cloudwatch_client(self, account_number):
        """
        Get CloudWatch client for the specified account
        
        Args:
            account_number (str): AWS Account number
        
        Returns:
            boto3.client: CloudWatch client
        """
        # If same account, use default CloudWatch client
        if account_number == self.current_account_id:
            return self.cloudwatch
        
        # Otherwise, assume role
        roles_table = self.dynamodb.Table(self.roles_table_name)
        role_response = roles_table.get_item(
            Key={'account': account_number}
        )
        
        role_arn = role_response['Item']['role']
        
        # Assume role
        assumed_role = self.sts_client.assume_role(
            RoleArn=role_arn,
            RoleSessionName='MetricDiscoverySession'
        )
        
        # Create CloudWatch client with assumed credentials
        return boto3.client(
            'cloudwatch', 
            aws_access_key_id=assumed_role['Credentials']['AccessKeyId'],
            aws_secret_access_key=assumed_role['Credentials']['SecretAccessKey'],
            aws_session_token=assumed_role['Credentials']['SessionToken']
        )

    def get_cached_metrics(self, namespace, account_number):
        """
        Retrieve cached metrics from S3 or fetch new if cache is stale
        
        Args:
            namespace (str): CloudWatch namespace
            account_number (str): AWS Account number
        
        Returns:
            list: List of metrics for the namespace
        """
        s3_key = f'metrics_cache/{account_number}/{namespace}_metrics.json'
        
        try:
            # Check S3 object metadata for last modified time
            s3_metadata = self.s3.head_object(
                Bucket=self.metrics_bucket, 
                Key=s3_key
            )
            
            # Check if file is older than 1 hour
            last_modified = s3_metadata['LastModified']
            if datetime.now(last_modified.tzinfo) - last_modified > timedelta(hours=1):
                raise Exception("Cache is stale")
            
            # Fetch cached metrics
            response = self.s3.get_object(
                Bucket=self.metrics_bucket, 
                Key=s3_key
            )
            return json.loads(response['Body'].read())
        
        except Exception as e:
            # Determine CloudWatch client based on account
            cloudwatch_client = self.get_cloudwatch_client(account_number)
            
            # Fetch new metrics if cache is stale or doesn't exist
            metrics = self.list_metrics_for_namespace(cloudwatch_client, namespace)
            
            # Cache metrics in S3
            self.s3.put_object(
                Bucket=self.metrics_bucket,
                Key=s3_key,
                Body=json.dumps(metrics)
            )
            
            return metrics

    def list_metrics_for_namespace(self, cloudwatch_client, namespace):
        """
        List metrics for a given namespace
        
        Args:
            cloudwatch_client (boto3.client): CloudWatch client
            namespace (str): CloudWatch namespace
        
        Returns:
            list: List of metrics for the namespace
        """
        # List metrics API call
        metrics_list = []
        paginator = cloudwatch_client.get_paginator('list_metrics')
        
        for response in paginator.paginate(Namespace=namespace):
            metrics_list.extend(response['Metrics'])
        
        return metrics_list

    def filter_metrics_by_resources_and_profile(self, metrics, namespace, account_number):
        """
        Filter metrics based on resources and profile configurations
        
        Args:
            metrics (list): List of metrics
            namespace (str): CloudWatch namespace
            account_number (str): AWS Account number
        
        Returns:
            list: Filtered metrics
        """
        # Get resources table
        resources_table = self.dynamodb.Table(self.resources_table_name)
        
        # Get profile table
        profile_table = self.dynamodb.Table(self.profile_table_name)
        
        # Scan resources for the given namespace and account
        resources_response = resources_table.scan(
            FilterExpression='namespace = :ns AND account = :acc',
            ExpressionAttributeValues={
                ':ns': namespace,
                ':acc': account_number
            }
        )
        
        # Scan profile for the given namespace
        profile_response = profile_table.scan(
            FilterExpression='namespace = :ns AND v2_flag = :flag',
            ExpressionAttributeValues={
                ':ns': namespace,
                ':flag': True
            }
        )
        
        # Extract ARNs and profile configurations
        resource_arns = [item['arn'] for item in resources_response['Items']]
        profile_configs = profile_response['Items']
        
        # Filter metrics
        filtered_metrics = []
        for metric in metrics:
            if metric['Dimensions'] and any(
                dim['Name'] == 'ResourceArn' and dim['Value'] in resource_arns 
                for dim in metric['Dimensions']
            ):
                filtered_metrics.append(metric)
        
        return filtered_metrics

    def generate_metrics_payload(self, filtered_metrics):
        """
        Generate payload for metrics data retrieval
        
        Args:
            filtered_metrics (list): List of filtered metrics
        
        Returns:
            dict: Payload for metrics data retrieval
        """
        payload = {
            'MetricDataQueries': []
        }
        
        for idx, metric in enumerate(filtered_metrics):
            payload['MetricDataQueries'].append({
                'Id': f'metric_{idx}',
                'MetricStat': {
                    'Metric': metric,
                    'Period': 300,  # 5-minute intervals
                    'Stat': 'Average'
                }
            })
        
        return payload

    def handler(self, event, context):
        """
        Lambda function handler
        
        Args:
            event (dict): Lambda event payload
            context (LambdaContext): Lambda context
        
        Returns:
            dict: Payload for metrics data retrieval
        """
        # Extract account number from payload
        account_number = event.get('account_number', self.current_account_id)
        
        # Get CloudWatch client for the account
        cloudwatch_client = self.get_cloudwatch_client(account_number)
        
        # Get profile table
        profile_table = self.dynamodb.Table(self.profile_table_name)
        
        # Scan for namespaces with v2 flag enabled
        profile_response = profile_table.scan(
            FilterExpression='v2_flag = :flag',
            ExpressionAttributeValues={
                ':flag': True
            }
        )
        
        # Aggregated metrics payload
        aggregated_payload = {'MetricDataQueries': []}
        
        # Process each namespace
        for profile in profile_response['Items']:
            namespace = profile['namespace']
            
            # Get cached or fresh metrics
            metrics = self.get_cached_metrics(namespace, account_number)
            
            # Filter metrics by resources
            filtered_metrics = self.filter_metrics_by_resources_and_profile(
                metrics, 
                namespace, 
                account_number
            )
            
            # Generate payload for this namespace
            namespace_payload = self.generate_metrics_payload(filtered_metrics)
            
            # Aggregate payloads
            aggregated_payload['MetricDataQueries'].extend(
                namespace_payload['MetricDataQueries']
            )
        
        return aggregated_payload

# Lambda handler export
def lambda_handler(event, context):
    handler = MetricDiscoveryHandler()
    return handler.handler(event, context)
