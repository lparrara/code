import software.amazon.awssdk.auth.credentials.EnvironmentVariableCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.sqs.SqsClient;
import software.amazon.awssdk.services.sqs.model.Message;
import software.amazon.awssdk.services.sqs.model.ReceiveMessageRequest;
import software.amazon.awssdk.services.sqs.model.ReceiveMessageResponse;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class RandomNumberConsumer {

    private static final String SQS_QUEUE_URL = System.getenv("SQS_QUEUE_URL");
    private static final String S3_BUCKET_NAME = System.getenv("S3_BUCKET_NAME");

    public static void main(String[] args) {
        // Initialize the SQS client
        SqsClient sqsClient = SqsClient.builder()
                .region(Region.US_EAST_1)  // Adjust the region as needed
                .credentialsProvider(EnvironmentVariableCredentialsProvider.create())
                .build();

        // Initialize the S3 client
        S3Client s3Client = S3Client.builder()
                .region(Region.US_EAST_1)  // Adjust the region as needed
                .credentialsProvider(EnvironmentVariableCredentialsProvider.create())
                .build();

        while (true) {
            try {
                // Receive messages from the SQS queue
                ReceiveMessageRequest receiveRequest = ReceiveMessageRequest.builder()
                        .queueUrl(SQS_QUEUE_URL)
                        .maxNumberOfMessages(1)
                        .build();

                ReceiveMessageResponse receiveResponse = sqsClient.receiveMessage(receiveRequest);
                List<Message> messages = receiveResponse.messages();

                for (Message message : messages) {
                    String messageBody = message.body();
                    System.out.println("Received message: " + messageBody);

                    // Save the message to an S3 bucket
                    saveMessageToS3(s3Client, messageBody);

                    // Delete the message from the queue after processing
                    sqsClient.deleteMessage(builder -> builder.queueUrl(SQS_QUEUE_URL).receiptHandle(message.receiptHandle()));
                }

                // Sleep for a while before polling again
                Thread.sleep(5000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void saveMessageToS3(S3Client s3Client, String message) {
        try {
            // Create a temporary file to store the message
            String tempFilePath = "/tmp/message.txt";
            Files.write(Paths.get(tempFilePath), message.getBytes());

            // Create a unique key for the S3 object
            String objectKey = "random-numbers/" + System.currentTimeMillis() + ".txt";

            // Put the object in the S3 bucket
            PutObjectRequest putRequest = PutObjectRequest.builder()
                    .bucket(S3_BUCKET_NAME)
                    .key(objectKey)
                    .build();

            s3Client.putObject(putRequest, Paths.get(tempFilePath));

            // Clean up the temporary file
            Files.delete(Paths.get(tempFilePath));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

