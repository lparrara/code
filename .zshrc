#!/bin/sh

#some useful options (man zshoptions)
#setopt autocd menucomplete

# history
HISTSIZE=9999
HISTFILE=~/.zsh_history
SAVEHIST=$HISTSIZE
HISTDUP=erase
setopt appendhistory
setopt sharehistory
setopt hist_ignore_space
setopt hist_ignore_all_dups
setopt hist_save_no_dups

ZSH_PATH=$HOME/.config/zsh

# source
source "$ZSH_PATH/aliases.zsh"
source "$ZSH_PATH/exports.zsh"
source "$ZSH_PATH/functions.zsh"

#zmodload zsh/complist

# plugins
source "$ZSH_PATH/plugins/zap-prompt/zap-prompt.zsh-theme"
source "$ZSH_PATH/plugins/zsh-completions/zsh-completions.plugin.zsh"
source "$ZSH_PATH/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh"
source "$ZSH_PATH/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
#source "$ZSH_PATH/plugins/supercharge/supercharge.plugin.zsh"
#source "$ZSH_PATH/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh"
source "$ZSH_PATH/plugins/fzf/fzf.plugin.zsh"
source "$ZSH_PATH/plugins/fzf-tab/fzf-tab.plugin.zsh"
source "$ZSH_PATH/plugins/vim/vim.plugin.zsh"

autoload -U compinit && compinit

zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
zstyle ':completion:*' menu no
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'ls --color $realpath'


#plug "hlisser/zsh-autopair"
#plug "zap-zsh/supercharge"
#plug "zap-zsh/vim"
#plug "zap-zsh/atmachine" 
#plug "zap-zsh/exa"

# Zsh to use the same colors as ls
#zstyle ':completion:*' list-colors 'di=34:ln=35:so=32:pi=33:ex=31:bd=46;34:cd=43;34:su=41;30:sg=46;30:tw=42;30:ow=43;30'
# keybinds
#bindkey '^ ' autosuggest-accept

#export PATH="$HOME/.local/bin":$PATH
#
#if command -v bat &> /dev/null; then
#  alias cat="bat -pp --theme \"Visual Studio Dark+\"" 
#  alias catt="bat --theme \"Visual Studio Dark+\"" 
#fi

#bindkey '^[[A' history-substring-search-up
#bindkey '^[[B' history-substring-search-down

#bindkey -M vicmd 'k' history-substring-search-up
#bindkey -M vicmd 'j' history-substring-search-down

eval "$(zoxide init zsh)"
