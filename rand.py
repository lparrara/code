import pyautogui
import time
import threading

def move_mouse():
    while True:
        try:
            x, y = pyautogui.position()
            new_x = x + 1 if random.randint(0, 1) else x - 1
            new_y = y + 1 if random.randint(0, 1) else y - 1
            pyautogui.moveTo(new_x, new_y)
        except:
            pass

        time.sleep(30)

threading.Thread(target=move_mouse).start()

while True:
    time.sleep(10000000)
