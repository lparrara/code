import boto3
import json
import logging
import os
from typing import Dict, Any, List
from botocore.exceptions import ClientError

# Configure logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)

def load_service_filters(bucket: str, key: str) -> Dict[str, bool]:
    """
    Load and parse the services configuration from S3.
    """
    try:
        s3_client = boto3.client('s3')
        response = s3_client.get_object(Bucket=bucket, Key=key)
        services_data = json.loads(response['Body'].read().decode('utf-8'))
        
        return {
            service['ServiceCode']: service['Active']
            for service in services_data.get('Services', [])
        }
    except ClientError as e:
        logger.error(f"Error reading from S3: {str(e)}")
        return {}
    except json.JSONDecodeError as e:
        logger.error(f"Error parsing services JSON: {str(e)}")
        return {}
    except Exception as e:
        logger.error(f"Unexpected error loading services: {str(e)}")
        return {}

def get_active_services(bucket: str, key: str) -> List[str]:
    """
    Get list of active service codes from S3 configuration.
    """
    services_map = load_service_filters(bucket, key)
    active_services = [code for code, active in services_map.items() if active]
    logger.info(f"Found {len(active_services)} active services")
    logger.info(json.dumps(active_services))
    return active_services

def handler(event: Dict[Any, Any], context: Any) -> Dict[str, Any]:
    """
    Lambda function to retrieve AWS service quotas for a specific account and region.
    """
    # Read environment variables
    table_name = os.environ.get('DYNAMODB_TABLE_NAME', 'roles')
    config_bucket = os.environ.get('CONFIG_S3_BUCKET')
    config_key = os.environ.get('CONFIG_S3_KEY')

    if not config_bucket or not config_key:
        logger.error("Missing S3 configuration environment variables")
        return {
            'statusCode': 500,
            'body': json.dumps({
                'error': 'Missing required environment variables: CONFIG_S3_BUCKET and CONFIG_S3_KEY'
            })
        }

    logger.info(f"Using DynamoDB table: {table_name}")
    logger.info(f"Reading configuration from s3://{config_bucket}/{config_key}")

    # Log the entire event for debugging
    logger.info(f"Received event: {json.dumps(event)}")
    
    try:
        # Input validation
        if not event:
            logger.error("Empty event received")
            return {
                'statusCode': 400,
                'body': json.dumps({
                    'error': 'Empty event received. No input provided.'
                })
            }
        
        # Check if this is a direct event or wrapped in a body
        if 'body' in event:
            try:
                body = json.loads(event['body'])
                logger.info(f"Parsed body: {body}")
            except json.JSONDecodeError:
                logger.error(f"Failed to parse body: {event['body']}")
                return {
                    'statusCode': 400,
                    'body': json.dumps({
                        'error': 'Invalid JSON in request body'
                    })
                }
        else:
            body = event
        
        # Validate required fields
        if not body.get('account-id') or not body.get('region'):
            logger.error(f"Missing required fields. Received: {body}")
            return {
                'statusCode': 400,
                'body': json.dumps({
                    'error': 'Invalid input. Account-id and region are required.',
                    'received': body
                })
            }
        
        account_number = body['account-id']
        region = body['region']
        
        logger.info(f"Processing for account: {account_number}, region: {region}")
        
        # Get active services from S3
        active_services = get_active_services(config_bucket, config_key)
        if not active_services:
            logger.warning("No active services found in configuration")
            return {
                'statusCode': 200,
                'body': json.dumps({
                    'account-id': account_number,
                    'region': region,
                    'services_and_quotas': [],
                    'warning': 'No active services found in configuration'
                })
            }

        # Initialize DynamoDB client
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table(table_name)
        
        # Retrieve role information from DynamoDB
        response = table.get_item(
            Key={
                'account-id': account_number,
                'region': region
            }
        )
        
        if 'Item' not in response:
            logger.error(f"No role found for account {account_number} in region {region}")
            return {
                'statusCode': 404,
                'body': json.dumps({
                    'error': f'No role found for account {account_number} in region {region}'
                })
            }
        
        role_name = response['Item']['name']
        
        # Assume the cross-account role
        sts_client = boto3.client('sts')
        assume_role_response = sts_client.assume_role(
            RoleArn=f'arn:aws:iam::{account_number}:role/{role_name}',
            RoleSessionName='QuotaCheckerSession'
        )
        
        # Create service quota client using assumed role credentials
        service_quota_client = boto3.client(
            'service-quotas',
            aws_access_key_id=assume_role_response['Credentials']['AccessKeyId'],
            aws_secret_access_key=assume_role_response['Credentials']['SecretAccessKey'],
            aws_session_token=assume_role_response['Credentials']['SessionToken'],
            region_name=region
        )
       
        # Retrieve services and quotas for this assumed role
        cross_account_services = []
        
        # Only process active services
        for service_code in active_services:
            try:
                # Get service details
                service_response = service_quota_client.list_service_quotas(ServiceCode=service_code)
                print(json.dumps(service_response['Quotas']))
                #service = service_response['Service']
                #
                ## Retrieve quotas for this service
                #quota_paginator = service_quota_client.get_paginator('list_service_quotas')
                #service_quotas = []
                #
                #for quota_page in quota_paginator.paginate(ServiceCode=service_code):
                #    print(quota_page)
                #    for quota in quota_page['Quotas']:
                #        service_quotas.append({
                #            'quota_name': quota['QuotaName'],
                #            'quota_value': quota['Value'],
                #            'quota_arn': quota['QuotaArn']
                #        })
                #
                ## Combine service info with its quotas
                #cross_account_services.append({
                #    'service_code': service['ServiceCode'],
                #    'service_name': service['ServiceName'],
                #    'quotas': service_quotas
                #})

            except Exception as e:
                logger.warning(f"Error processing service {service_code}: {str(e)}")
                continue

        return {
            'statusCode': 200,
            'body': json.dumps({
                'account-id': account_number,
                'region': region,
                'services_and_quotas': cross_account_services,
                'total_services_processed': len(cross_account_services)
            })
        }
    
    except Exception as e:
        logger.error(f"Unexpected error: {str(e)}", exc_info=True)
        return {
            'statusCode': 500,
            'body': json.dumps({
                'error': str(e),
                'event': event
            })
        }
