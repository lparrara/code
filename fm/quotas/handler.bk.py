import boto3
import json
import logging
import os
from typing import Dict, Any

# Configure logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)

def handler(event: Dict[Any, Any], context: Any) -> Dict[str, Any]:
    """
    Lambda function to retrieve AWS service quotas for a specific account and region.
    """
    # Read table name from environment variable with a default
    table_name = os.environ.get('DYNAMODB_TABLE_NAME', 'roles')
    logger.info(f"Using DynamoDB table: {table_name}")

    # Log the entire event for debugging
    logger.info(f"Received event: {json.dumps(event)}")
    
    try:
        # More robust input validation
        if not event:
            logger.error("Empty event received")
            return {
                'statusCode': 400,
                'body': json.dumps({
                    'error': 'Empty event received. No input provided.'
                })
            }
        
        # Check if this is a direct event or wrapped in a body (common in API Gateway)
        if 'body' in event:
            try:
                # Try to parse the body if it's a JSON string
                body = json.loads(event['body'])
                logger.info(f"Parsed body: {body}")
            except json.JSONDecodeError:
                logger.error(f"Failed to parse body: {event['body']}")
                return {
                    'statusCode': 400,
                    'body': json.dumps({
                        'error': 'Invalid JSON in request body'
                    })
                }
        else:
            body = event
        
        # Validate required fields using the keys from the actual payload
        if not body.get('account-id') or not body.get('region'):
            logger.error(f"Missing required fields. Received: {body}")
            return {
                'statusCode': 400,
                'body': json.dumps({
                    'error': 'Invalid input. Account-id and region are required.',
                    'received': body
                })
            }
        
        account_number = body['account-id']
        region = body['region']
        
        logger.info(f"Processing for account: {account_number}, region: {region}")
        
        # Initialize DynamoDB client to look up role
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table(table_name)
        
        # Retrieve role information from DynamoDB
        response = table.get_item(
            Key={
                'account-id': account_number,
                'region': region
            }
        )
        
        if 'Item' not in response:
            logger.error(f"No role found for account {account_number} in region {region}")
            return {
                'statusCode': 404,
                'body': json.dumps({
                    'error': f'No role found for account {account_number} in region {region}'
                })
            }
        
        role_name = response['Item']['name']
        
        # Assume the cross-account role
        sts_client = boto3.client('sts')
        assume_role_response = sts_client.assume_role(
            RoleArn=f'arn:aws:iam::{account_number}:role/{role_name}',
            RoleSessionName='QuotaCheckerSession'
        )
        
        # Create service quota client using assumed role credentials
        service_quota_client = boto3.client(
            'service-quotas',
            aws_access_key_id=assume_role_response['Credentials']['AccessKeyId'],
            aws_secret_access_key=assume_role_response['Credentials']['SecretAccessKey'],
            aws_session_token=assume_role_response['Credentials']['SessionToken'],
            region_name=region
        )
       
        # Retrieve services and quotas for this assumed role
        cross_account_services = []
        paginator = service_quota_client.get_paginator('list_services')
        for page in paginator.paginate():
            for service in page['Services']:
                # Retrieve quotas for this service
                quota_paginator = service_quota_client.get_paginator('list_service_quotas')
                service_quotas = []
                
                for quota_page in quota_paginator.paginate(ServiceCode=service['ServiceCode']):
                    for quota in quota_page['Quotas']:
                        service_quotas.append({
                            'quota_name': quota['QuotaName'],
                            'quota_value': quota['Value'],
                            'quota_arn': quota['QuotaArn']
                        })
                
                # Combine service info with its quotas
                cross_account_services.append({
                    'service_code': service['ServiceCode'],
                    'service_name': service['ServiceName'],
                    'quotas': service_quotas
                })

                print(cross_account_services)
        
#        # Add to cross-account results
#        response_data['cross_account'].append({
#            'account_id': account_id,
#            'region': role_region,
#            'services_and_quotas': cross_account_services
#        })

#        # Retrieve service quotas
#        quotas = []
#        paginator = service_quota_client.get_paginator('list_service_quotas')
#        
#        for page in paginator.paginate():
#            for quota in page['Quotas']:
#                quotas.append({
#                    'service_code': quota['ServiceCode'],
#                    'quota_name': quota['QuotaName'],
#                    'value': quota['Value'],
#                    'unit': quota.get('Unit', 'N/A')
#                })
#        
#        return {
#            'statusCode': 200,
#            'body': json.dumps({
#                'account-id': account_number,
#                'region': region,
#                'quotas': quotas
#            })
#        }
    
    except Exception as e:
        logger.error(f"Unexpected error: {str(e)}", exc_info=True)
        return {
            'statusCode': 500,
            'body': json.dumps({
                'error': str(e),
                'event': event
            })
        }
