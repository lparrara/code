from fastapi import FastAPI, HTTPException, Depends, Query
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel, Field
from typing import List, Dict, Optional
from datetime import datetime
import uuid

# Initialize FastAPI app
app = FastAPI(
    title="Log Group Tracking API",
    description="API for tracking log groups, data points, and destinations",
    version="1.0.0"
)

# Add CORS middleware
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Data models
class DataPoint(BaseModel):
    timestamp: datetime
    size: int  # Size in bytes

class Destination(BaseModel):
    id: str = Field(default_factory=lambda: str(uuid.uuid4()))
    name: str
    type: str  # e.g., "s3", "cloudwatch", "elasticsearch"
    config: Dict  # Configuration details for the destination

class LogGroup(BaseModel):
    id: str = Field(default_factory=lambda: str(uuid.uuid4()))
    name: str
    created_at: datetime = Field(default_factory=datetime.now)
    updated_at: datetime = Field(default_factory=datetime.now)
    datapoints: List[DataPoint] = []
    destinations: List[Destination] = []
    active: bool = True
    retention_days: Optional[int] = None

# In-memory database (for demo purposes)
log_groups: Dict[str, LogGroup] = {}

# Create a new log group
@app.post("/log-groups/", response_model=LogGroup)
async def create_log_group(log_group: LogGroup):
    if log_group.id in log_groups:
        raise HTTPException(status_code=400, detail=f"Log group with ID {log_group.id} already exists")
    log_groups[log_group.id] = log_group
    return log_group

# Get all log groups
@app.get("/log-groups/", response_model=List[LogGroup])
async def get_log_groups(
    active: Optional[bool] = Query(None, description="Filter by active status"),
    destination_type: Optional[str] = Query(None, description="Filter by destination type")
):
    filtered_groups = log_groups.values()
    
    if active is not None:
        filtered_groups = [group for group in filtered_groups if group.active == active]
    
    if destination_type:
        filtered_groups = [
            group for group in filtered_groups 
            if any(dest.type == destination_type for dest in group.destinations)
        ]
    
    return list(filtered_groups)

# Get a specific log group by ID
@app.get("/log-groups/{log_group_id}", response_model=LogGroup)
async def get_log_group(log_group_id: str):
    if log_group_id not in log_groups:
        raise HTTPException(status_code=404, detail=f"Log group with ID {log_group_id} not found")
    return log_groups[log_group_id]

# Update a log group
@app.put("/log-groups/{log_group_id}", response_model=LogGroup)
async def update_log_group(log_group_id: str, updated_log_group: LogGroup):
    if log_group_id not in log_groups:
        raise HTTPException(status_code=404, detail=f"Log group with ID {log_group_id} not found")
    
    # Preserve the original ID and created_at
    original_created_at = log_groups[log_group_id].created_at
    updated_log_group.id = log_group_id
    updated_log_group.created_at = original_created_at
    updated_log_group.updated_at = datetime.now()
    
    log_groups[log_group_id] = updated_log_group
    return updated_log_group

# Delete a log group
@app.delete("/log-groups/{log_group_id}", response_model=dict)
async def delete_log_group(log_group_id: str):
    if log_group_id not in log_groups:
        raise HTTPException(status_code=404, detail=f"Log group with ID {log_group_id} not found")
    del log_groups[log_group_id]
    return {"message": f"Log group with ID {log_group_id} deleted successfully"}

# Add a data point to a log group
class DataPointCreate(BaseModel):
    timestamp: datetime = Field(default_factory=datetime.now)
    size: int

@app.post("/log-groups/{log_group_id}/datapoints", response_model=LogGroup)
async def add_datapoint(log_group_id: str, datapoint: DataPointCreate):
    if log_group_id not in log_groups:
        raise HTTPException(status_code=404, detail=f"Log group with ID {log_group_id} not found")
    
    log_group = log_groups[log_group_id]
    log_group.datapoints.append(DataPoint(timestamp=datapoint.timestamp, size=datapoint.size))
    log_group.updated_at = datetime.now()
    
    return log_group

# Add a destination to a log group
@app.post("/log-groups/{log_group_id}/destinations", response_model=LogGroup)
async def add_destination(log_group_id: str, destination: Destination):
    if log_group_id not in log_groups:
        raise HTTPException(status_code=404, detail=f"Log group with ID {log_group_id} not found")
    
    log_group = log_groups[log_group_id]
    log_group.destinations.append(destination)
    log_group.updated_at = datetime.now()
    
    return log_group

# Remove a destination from a log group
@app.delete("/log-groups/{log_group_id}/destinations/{destination_id}", response_model=LogGroup)
async def remove_destination(log_group_id: str, destination_id: str):
    if log_group_id not in log_groups:
        raise HTTPException(status_code=404, detail=f"Log group with ID {log_group_id} not found")
    
    log_group = log_groups[log_group_id]
    
    # Find and remove the destination
    for i, dest in enumerate(log_group.destinations):
        if dest.id == destination_id:
            log_group.destinations.pop(i)
            log_group.updated_at = datetime.now()
            return log_group
    
    raise HTTPException(status_code=404, detail=f"Destination with ID {destination_id} not found in log group")

# Get log group statistics
class LogGroupStats(BaseModel):
    total_size: int
    datapoint_count: int
    first_datapoint: Optional[datetime] = None
    last_datapoint: Optional[datetime] = None
    average_size: float
    destination_count: int

@app.get("/log-groups/{log_group_id}/stats", response_model=LogGroupStats)
async def get_log_group_stats(log_group_id: str):
    if log_group_id not in log_groups:
        raise HTTPException(status_code=404, detail=f"Log group with ID {log_group_id} not found")
    
    log_group = log_groups[log_group_id]
    datapoints = log_group.datapoints
    
    if not datapoints:
        return LogGroupStats(
            total_size=0,
            datapoint_count=0,
            average_size=0,
            destination_count=len(log_group.destinations)
        )
    
    # Sort datapoints by timestamp
    sorted_datapoints = sorted(datapoints, key=lambda x: x.timestamp)
    
    total_size = sum(dp.size for dp in datapoints)
    datapoint_count = len(datapoints)
    
    return LogGroupStats(
        total_size=total_size,
        datapoint_count=datapoint_count,
        first_datapoint=sorted_datapoints[0].timestamp if datapoints else None,
        last_datapoint=sorted_datapoints[-1].timestamp if datapoints else None,
        average_size=total_size / datapoint_count if datapoint_count > 0 else 0,
        destination_count=len(log_group.destinations)
    )

# Query datapoints by time range
@app.get("/log-groups/{log_group_id}/datapoints", response_model=List[DataPoint])
async def query_datapoints(
    log_group_id: str, 
    start_time: Optional[datetime] = Query(None, description="Start timestamp for filtering datapoints"),
    end_time: Optional[datetime] = Query(None, description="End timestamp for filtering datapoints"),
    limit: int = Query(100, description="Maximum number of datapoints to return")
):
    if log_group_id not in log_groups:
        raise HTTPException(status_code=404, detail=f"Log group with ID {log_group_id} not found")
    
    log_group = log_groups[log_group_id]
    datapoints = log_group.datapoints
    
    if start_time:
        datapoints = [dp for dp in datapoints if dp.timestamp >= start_time]
    if end_time:
        datapoints = [dp for dp in datapoints if dp.timestamp <= end_time]
    
    # Sort by timestamp (newest first) and limit the results
    sorted_datapoints = sorted(datapoints, key=lambda x: x.timestamp, reverse=True)
    return sorted_datapoints[:limit]

# If you need to run the server
if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
