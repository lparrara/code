import requests
import json
from datetime import datetime

# Base URL for the API
BASE_URL = "http://localhost:8000"  # Adjust if your API is hosted elsewhere

# Helper function to format datetime objects for JSON serialization
def datetime_to_iso(dt):
    if isinstance(dt, datetime):
        return dt.isoformat()
    return dt

# Helper function to handle JSON serialization of datetime objects
def json_serialize(obj):
    return json.dumps(obj, default=datetime_to_iso)

# Create a new log group
def create_log_group(name, retention_days=None):
    url = f"{BASE_URL}/log-groups/"
    data = {
        "name": name,
        "created_at": datetime.now(),
        "updated_at": datetime.now(),
        "datapoints": [],
        "destinations": [],
        "active": True,
        "retention_days": retention_days
    }
    
    response = requests.post(url, data=json_serialize(data), headers={"Content-Type": "application/json"})
    return response.json() if response.status_code == 200 else {"error": response.text, "status_code": response.status_code}

# Get all log groups
def get_log_groups(active=None, destination_type=None):
    url = f"{BASE_URL}/log-groups/"
    params = {}
    
    if active is not None:
        params["active"] = active
    if destination_type:
        params["destination_type"] = destination_type
    
    response = requests.get(url, params=params)
    return response.json() if response.status_code == 200 else {"error": response.text, "status_code": response.status_code}

# Get a specific log group by ID
def get_log_group(log_group_id):
    url = f"{BASE_URL}/log-groups/{log_group_id}"
    response = requests.get(url)
    return response.json() if response.status_code == 200 else {"error": response.text, "status_code": response.status_code}

# Update a log group
def update_log_group(log_group_id, updated_data):
    url = f"{BASE_URL}/log-groups/{log_group_id}"
    
    # First get the current data to make sure we're not missing required fields
    current_data = get_log_group(log_group_id)
    if "error" in current_data:
        return current_data
    
    # Update the data
    for key, value in updated_data.items():
        current_data[key] = value
    
    # Set the updated_at timestamp
    current_data["updated_at"] = datetime.now()
    
    response = requests.put(url, data=json_serialize(current_data), headers={"Content-Type": "application/json"})
    return response.json() if response.status_code == 200 else {"error": response.text, "status_code": response.status_code}

# Delete a log group
def delete_log_group(log_group_id):
    url = f"{BASE_URL}/log-groups/{log_group_id}"
    response = requests.delete(url)
    return response.json() if response.status_code == 200 else {"error": response.text, "status_code": response.status_code}

# Add a data point to a log group
def add_datapoint(log_group_id, size, timestamp=None):
    url = f"{BASE_URL}/log-groups/{log_group_id}/datapoints"
    data = {
        "size": size,
        "timestamp": timestamp or datetime.now()
    }
    
    response = requests.post(url, data=json_serialize(data), headers={"Content-Type": "application/json"})
    return response.json() if response.status_code == 200 else {"error": response.text, "status_code": response.status_code}

# Add a destination to a log group
def add_destination(log_group_id, name, type, config):
    url = f"{BASE_URL}/log-groups/{log_group_id}/destinations"
    data = {
        "name": name,
        "type": type,
        "config": config
    }
    
    response = requests.post(url, data=json_serialize(data), headers={"Content-Type": "application/json"})
    return response.json() if response.status_code == 200 else {"error": response.text, "status_code": response.status_code}

# Remove a destination from a log group
def remove_destination(log_group_id, destination_id):
    url = f"{BASE_URL}/log-groups/{log_group_id}/destinations/{destination_id}"
    response = requests.delete(url)
    return response.json() if response.status_code == 200 else {"error": response.text, "status_code": response.status_code}

# Get log group statistics
def get_log_group_stats(log_group_id):
    url = f"{BASE_URL}/log-groups/{log_group_id}/stats"
    response = requests.get(url)
    return response.json() if response.status_code == 200 else {"error": response.text, "status_code": response.status_code}

# Query datapoints by time range
def query_datapoints(log_group_id, start_time=None, end_time=None, limit=100):
    url = f"{BASE_URL}/log-groups/{log_group_id}/datapoints"
    params = {"limit": limit}
    
    if start_time:
        params["start_time"] = start_time.isoformat() if isinstance(start_time, datetime) else start_time
    if end_time:
        params["end_time"] = end_time.isoformat() if isinstance(end_time, datetime) else end_time
    
    response = requests.get(url, params=params)
    return response.json() if response.status_code == 200 else {"error": response.text, "status_code": response.status_code}


# Example usage
if __name__ == "__main__":
    # Create a new log group
    print("Creating a new log group...")
    log_group = create_log_group("application-logs", retention_days=30)
    log_group_id = log_group["id"]
    print(f"Created log group with ID: {log_group_id}")
    
    # Add some data points
    print("Adding data points...")
    add_datapoint(log_group_id, 1024)  # 1KB
    add_datapoint(log_group_id, 2048)  # 2KB
    
    # Add a destination
    print("Adding a destination...")
    s3_config = {
        "bucket": "logs-bucket",
        "prefix": "application-logs/",
        "region": "us-west-2"
    }
    add_destination(log_group_id, "S3 Backup", "s3", s3_config)
    
    # Get updated log group
    updated_log_group = get_log_group(log_group_id)
    print(f"Log group now has {len(updated_log_group['datapoints'])} data points and {len(updated_log_group['destinations'])} destinations")
    
    # Get statistics
    stats = get_log_group_stats(log_group_id)
    print(f"Log group statistics: {stats}")
    
    # Update the log group
    print("Updating log group...")
    update_log_group(log_group_id, {"active": False})
    
    # Query all inactive log groups
    inactive_groups = get_log_groups(active=False)
    print(f"Found {len(inactive_groups)} inactive log groups")
    
    # Clean up - uncomment to delete the log group
    # print("Deleting log group...")
    # delete_log_group(log_group_id)
