package com.example.producer;

import java.util.HashMap;
//import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;

import io.opentelemetry.api.trace.Span;
import io.opentelemetry.instrumentation.annotations.WithSpan;

public class Handler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

  @Override
  @WithSpan("OpenTelemetry Example")
  public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent event, Context context) {
    String trace_id = Span.current().getSpanContext().getTraceId();

    APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();
    response.setStatusCode(200);
    HashMap<String, String> headers = new HashMap<String, String>();
    headers.put("Content-Type", "text/html");
    response.setHeaders(headers);
    String body = "java17";
    response.setBody("<!DOCTYPE html><html><head><title>" + body + "</title></head><body>" +
        "<h1>Welcome</h1><p>.</p>" +
        "<p>TraceId: <a href=\"https://fnma-mars-production.signalfx.com/#/apm/traces/" + trace_id + "\"" + trace_id
        + "</a>" +
        "</body></html>");
    System.out.println(response);

    RandomNumberProducer.produce();
    // producer = new RandomNumberProducer();
    // producer.produce();

    return response;
  }
}
