package com.example.producer;

//import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.PublishRequest;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Random;

public class RandomNumberProducer {

  public static void produce() {
    // Load properties from file
    Properties properties = new Properties();
    try {
      properties.load(new FileInputStream("config.properties"));
    } catch (IOException e) {
      e.printStackTrace();
      return;
    }

    // Get SNS topic ARN from properties
    String snsTopicArn = properties.getProperty("snsTopicArn");

    // Generate random number
    Random random = new Random();
    int randomNumber = random.nextInt(1000);

    // Publish the random number to the SNS topic
    publishToSNS(snsTopicArn, String.valueOf(randomNumber), properties.getProperty("endpoint"));
  }

  private static void publishToSNS(String topicArn, String message, String endpoint) {
    // Set up AWS credentials and SNS client
    AmazonSNSClient snsClient;
    BasicSessionCredentials awsCredentials = new BasicSessionCredentials("accessKey", "secretKey", "sessionToken");

    // If endpoint is provided in properties, use it; otherwise, use default AWS
    // endpoints
    if (endpoint != null && !endpoint.isEmpty()) {
      snsClient = new AmazonSNSClient(awsCredentials);
      snsClient.setEndpoint(endpoint);
    } else {
      snsClient = new AmazonSNSClient();
    }

    // Publish message to SNS topic
    snsClient.publish(new PublishRequest().withTopicArn(topicArn).withMessage(message));
  }
}
