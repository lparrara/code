package com.example.producer;

import java.util.HashMap;
import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;

import io.opentelemetry.api.trace.Span;
import io.opentelemetry.instrumentation.annotations.WithSpan;

import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.Bucket;
import software.amazon.awssdk.services.s3.model.ListBucketsResponse;

public class Handler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

  @Override
  @WithSpan("OpenTelemetry Example")
  public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent event, Context context) {
    // Region region = Region.US_EAST_1;
    // S3Client s3 = S3Client.builder()
    // .region(region)
    // .build();
    //
    // listAllBuckets(s3);

    // RollDice roll_dice = new RollDice();
    // roll_dice.play();
    // RollDiceSpan roll_dice_span = new RollDiceSpan();
    // roll_dice_span.play();
    // RollDiceSpanAttribute roll_dice_span_attribute = new RollDiceSpanAttribute();
    // roll_dice_span_attribute.play();

    String trace_id = Span.current().getSpanContext().getTraceId();

    APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();
    response.setStatusCode(200);
    HashMap<String, String> headers = new HashMap<String, String>();
    headers.put("Content-Type", "text/html");
    response.setHeaders(headers);
    String body = "java17";
    response.setBody("<!DOCTYPE html><html><head><title>" + body + "</title></head><body>" +
        "<h1>Welcome</h1><p>.</p>" +
        "<p>TraceId: <a href=\"https://fnma-mars-production.signalfx.com/#/apm/traces/" + trace_id + "\"" + trace_id
        + "</a>" +
        "</body></html>");
    System.out.println(response);

    RandomNumberProducer producer = new RandomNumberProducer();
    producer.produce();

    return response;
  }

  public static void listAllBuckets(S3Client s3) {
    ListBucketsResponse response = s3.listBuckets();
    List<Bucket> bucketList = response.buckets();
    for (Bucket bucket : bucketList) {
      System.out.println(" " + bucket.name());
    }
  }
}
