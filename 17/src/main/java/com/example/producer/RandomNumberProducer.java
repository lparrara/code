package com.example.producer;

import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sns.SnsClient;
import software.amazon.awssdk.services.sns.model.PublishRequest;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Random;
import java.net.URI;

public class RandomNumberProducer {

  public static void produce() {
    // Load properties from file
    Properties properties = new Properties();
    try {
      properties.load(new FileInputStream("config.properties"));
    } catch (IOException e) {
      e.printStackTrace();
      return;
    }

    // Get SNS topic ARN from properties
    String snsTopicArn = properties.getProperty("snsTopicArn");

    // Generate random number
    Random random = new Random();
    int randomNumber = random.nextInt(1000);

    // Publish the random number to the SNS topic
    publishToSNS(snsTopicArn, String.valueOf(randomNumber), properties.getProperty("endpoint"));
  }

  private static void publishToSNS(String topicArn, String message, String endpoint) {
    // Set up AWS credentials and SNS client
    AwsBasicCredentials awsCredentials = AwsBasicCredentials.create("accessKey", "secretKey");
    SnsClient snsClient;
    // If endpoint is provided in properties, use it; otherwise, use default AWS
    // endpoints
    if (endpoint != null && !endpoint.isEmpty()) {
      snsClient = SnsClient.builder()
          .region(Region.US_EAST_1)
          .credentialsProvider(StaticCredentialsProvider.create(awsCredentials))
          .endpointOverride(URI.create(endpoint))
          .build();
    } else {
      snsClient = SnsClient.builder()
          .region(Region.US_EAST_1)
          .credentialsProvider(StaticCredentialsProvider.create(awsCredentials))
          .build();
    }

    // Publish message to SNS topic
    snsClient.publish(PublishRequest.builder().topicArn(topicArn).message(message).build());
  }
}
