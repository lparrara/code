package com.example.consumer;

import com.amazonaws.auth.AWSStaticCredentialsProvider;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class RandomNumberConsumer {

  public static void readMessage() {
    // Load properties from file
    Properties properties = new Properties();
    try {
      properties.load(new FileInputStream("config.properties"));
    } catch (IOException e) {
      e.printStackTrace();
      return;
    }

    // Create an SQS client
    String SQS_QUEUE_URL = properties.getProperty("sqsQueue");
    BasicAWSCredentials credentials = new BasicAWSCredentials("ACCESS_KEY_ID", "SECRET_KEY");
    AmazonSQSClient sqsClient = new AmazonSQSClient(credentials);

    // Set up the receive message request
    ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest();
    receiveMessageRequest.setQueueUrl(SQS_QUEUE_URL);
    receiveMessageRequest.setMaxNumberOfMessages(10); // wait for 10 seconds before timing out

    // Get the messages from SQS
    com.amazonaws.services.sqs.model.ReceiveMessageResult result = sqsClient.receiveMessage(receiveMessageRequest);

    // Process each message
    for (Message message : result.getMessages()) {
      System.out.println("Received message: " + message.getBody());
      // Do something with the message, such as processing it or storing it in a
      // database

      // Delete the message from SQS to avoid re-processing it
      sqsClient.deleteMessage(SQS_QUEUE_URL, message.getReceiptHandle());
    }
  }
}
